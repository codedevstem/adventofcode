package no.cds.adventofcode.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class IO {
  public static BufferedReader readFile(String path) {
    try {
      return new BufferedReader(
        new FileReader(new File(path).getAbsolutePath()));
    } catch (FileNotFoundException e) {
      System.out.println("File could not be found. Error code: " + e.getMessage());
    }
    return null;
  }

}
