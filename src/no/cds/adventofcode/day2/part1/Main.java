package no.cds.adventofcode.day2.part1;

import no.cds.adventofcode.util.IO;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    BufferedReader bufferedReader = IO.readFile("src/no/cds/adventofcode/day2/part1/input.txt");
    int sequenceWithTwo = 0;
    int sequenceWithThree = 0;
    String line;
    try {
      while ((line = bufferedReader.readLine()) != null) {
        List<String> chars = Arrays.asList(line.split(""));
        Collections.sort(chars);
        Map<String, Integer> hashMap = new HashMap<>();
        chars.forEach(e -> {
          if(hashMap.containsKey(e)) {
            hashMap.put(e, hashMap.get(e) + 1);
          } else {
            hashMap.put(e, 1);
          }
        });
        hashMap.entrySet().removeIf(stringIntegerEntry -> stringIntegerEntry.getValue() == 1 || stringIntegerEntry.getValue() > 3);
        int withThree = Collections.frequency(hashMap.values(), 3);
        int withTwo = Collections.frequency(hashMap.values(), 2);
        if(withThree >= 1 && withTwo >= 1) {
          sequenceWithThree++;
          sequenceWithTwo++;
        } else if (withThree >= 1) {
          sequenceWithThree++;
        } else if(withTwo >= 1) {
          sequenceWithTwo++;
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Sequence of two: " + sequenceWithTwo + "\n" +
      "Sequence of Three: " + sequenceWithThree + "\n" +
      "Checksum is: " + sequenceWithThree * sequenceWithTwo);
  }
}
