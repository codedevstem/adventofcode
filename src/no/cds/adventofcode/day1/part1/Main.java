package no.cds.adventofcode.day1.part1;

import no.cds.adventofcode.util.IO;

import java.io.BufferedReader;

public class Main {
  public static void main(String[] args) {
    BufferedReader bufferedReader = IO.readFile("src/no/cds/adventofcode/day1/part1/input.txt");
    System.out.println(bufferedReader.lines().mapToInt(Integer::valueOf).sum());
  }
}
