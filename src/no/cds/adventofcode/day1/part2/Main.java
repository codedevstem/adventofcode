package no.cds.adventofcode.day1.part2;

import no.cds.adventofcode.util.IO;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
  public static void main(String[] args) {
    BufferedReader bufferedReader = IO.readFile("src/no/cds/adventofcode/day1/part2/input.txt");
    List<Integer> foundNumbers = new ArrayList<>();
    List<Integer> numbers;
    int number = 0;
    boolean isFound = false;
    if (bufferedReader != null) {
      numbers = bufferedReader.lines().map(Integer::valueOf).collect(Collectors.toList());
      int sum = 0;
      while (!isFound) {
        for (int x : numbers) {
          sum += x;
          if (foundNumbers.contains(sum)) {
            isFound = true;
            number = sum;
            break;
          }
          foundNumbers.add(sum);
        }
      }
    }
    System.out.println("Number was found: " + number);
  }
}
